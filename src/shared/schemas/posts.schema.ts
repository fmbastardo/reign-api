import { Prop, Schema, SchemaFactory, SchemaOptions } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import mongoose from 'mongoose';

export type PostDocument = Posts & Document;

@Schema()
export class Posts {
  // @Prop()
  // _id: mongoose.Schema.Types.ObjectId;

  @Prop()
  author: string;

  @Prop([String])
  _tags: string[];

  @Prop()
  title: string;

  @Prop()
  created_at_i: Date;

  @Prop()
  objectID: string;

  @Prop()
  url: string;

  @Prop()
  comment_text: string;

  @Prop()
  story_title: string;

  @Prop()
  story_url: string;
}

export const PostSchema = SchemaFactory.createForClass(Posts);
