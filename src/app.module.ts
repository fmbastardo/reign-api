import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduledTaskModule } from './scheduled-task/scheduled-task.module';
import { UserCrudModule } from './user-crud/user-crud.module';

@Module({
  imports: [
    ScheduledTaskModule,
    UserCrudModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService) => ({
        uri: configService.get('MONGOURI'),
      }),
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
