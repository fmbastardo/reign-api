import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { lastValueFrom, Observable } from 'rxjs';
import { IPosts } from '../../scheduled-task/dto/posts.interface';

@Injectable()
export class PostsService {
  constructor(private httpService: HttpService) {}

  async getPosts(searchtime): Promise<AxiosResponse<IPosts>> {
    const posts = await lastValueFrom(
      this.httpService.get('https://hn.algolia.com/api/v1/search_by_date', {
        params: {
          query: 'nodejs',
          numericFilters: `created_at_i>${searchtime}`,
        },
      }),
    );
    return posts;
  }
}
