import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { PostsService } from './posts.service';
import { PostDocument } from '../../shared/schemas/posts.schema';
import { PostsRepository } from '../repositories/posts.repository';

@Injectable()
export class ScheduleTaskService {
  constructor(
    private postsService: PostsService,
    private readonly postsRepository: PostsRepository,
  ) {}

  @Cron('0 0 * * * *')
  async handleCron() {
    const ONEHOURAGO = Date.now() - 3600000;
    const ONEHOURAGOINSEC = (ONEHOURAGO / 1000).toFixed(0);
    const posts = await this.postsService.getPosts(ONEHOURAGOINSEC.toString());
    const length = posts.data.hits.length;
    if (length > 0) {
      for (let doc = 0; doc < length; doc++) {
        this.postsRepository.create(posts.data.hits[length - 1]);
      }
    }
  }
}
