import { Injectable } from '@nestjs/common';
import { PostDocument, Posts } from '../../shared/schemas/posts.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { IHit } from '../dto/posts.interface';

@Injectable()
export class PostsRepository {
  constructor(
    @InjectModel(Posts.name) private postModel: Model<PostDocument>,
  ) {}

  async findOne(iHit: IHit): Promise<Posts> {
    return this.postModel.findOne({ objectID: iHit.objectID }).exec();
  }

  async create(iHit: IHit): Promise<Posts> {
    const createPost = new this.postModel(iHit);
    return createPost.save();
  }
}
