import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { ScheduleTaskService } from './services/schedule-task.service';
import { PostsService } from './services/posts.service';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PostsRepository } from './repositories/posts.repository';
import mongoose from 'mongoose';
import { MongooseModule } from '@nestjs/mongoose';
import { Posts, PostSchema } from '../shared/schemas/posts.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Posts.name, schema: PostSchema }]),
    ScheduleModule.forRoot(),
    HttpModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        timeout: configService.get('HTTP_TIMEOUT'),
        maxRedirects: configService.get('HTTP_MAX_REDIRECTS'),
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [ScheduleTaskService, PostsService, PostsRepository],
})
export class ScheduledTaskModule {}
