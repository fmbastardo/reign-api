export interface IGetRequest {
  _tags: string | string[];
  title: string;
  author: string | string[];
  page: number;
}
