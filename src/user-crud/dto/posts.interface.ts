export interface IAuthor {
  value: string;
  matchLevel: string;
  matchedWords: any[];
}

export interface ICommentText {
  value: string;
  matchLevel: string;
  fullyHighlighted: boolean;
  matchedWords: string[];
}

export interface IStoryTitle {
  value: string;
  matchLevel: string;
  matchedWords: any[];
}

export interface IStoryUrl {
  value: string;
  matchLevel: string;
  matchedWords: any[];
}

export interface IHighlightResult {
  author: IAuthor;
  comment_text: ICommentText;
  story_title: IStoryTitle;
  story_url: IStoryUrl;
}

export interface IHit {
  created_at: Date;
  title?: any;
  url?: any;
  author: string;
  points?: any;
  story_text?: any;
  comment_text: string;
  num_comments?: any;
  story_id: number;
  story_title: string;
  story_url: string;
  parent_id: number;
  created_at_i: number;
  _tags: string[];
  objectID: string;
  _highlightResult: IHighlightResult;
}

export interface IPosts {
  hits: IHit[];
  nbHits: number;
  page: number;
  nbPages: number;
  hitsPerPage: number;
  exhaustiveNbHits: boolean;
  exhaustiveTypo: boolean;
  query: string;
  params: string;
  processingTimeMS: number;
}
