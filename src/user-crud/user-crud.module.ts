import { Module } from '@nestjs/common';
import { UserCrudService } from './service/user-crud.service';
import { UserCrudController } from './controllers/user-crud.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { PostSchema, Posts } from '../shared/schemas/posts.schema';
import { UserCrudRepository } from './repositories/user-crud.repository';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Posts.name, schema: PostSchema }]),
  ],
  providers: [UserCrudService, UserCrudRepository],

  controllers: [UserCrudController],
})
export class UserCrudModule {}
