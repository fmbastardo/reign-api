import { Injectable, Controller } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Posts, PostDocument } from '../../shared/schemas/posts.schema';
import { IGetRequest } from '../dto/request.interface';
import { IHit } from '../../user-crud/dto/posts.interface';

@Injectable()
export class UserCrudRepository {
  constructor(
    @InjectModel(Posts.name) private postModel: Model<PostDocument>,
  ) {}

  async findOne(): Promise<Posts> {
    return;
  }

  async findAll(iGetRequest: IGetRequest): Promise<Posts[]> {
    const post = await this.postModel
      .find(iGetRequest)
      .limit(5)
      .skip(5 * iGetRequest.page)
      .exec();
    return post;
  }

  async create(ihit: IHit): Promise<Posts> {
    return await this.postModel.create(ihit);
  }

  async delete(_id: string): Promise<Posts> {
    return await this.postModel.findByIdAndDelete(_id);
  }
}
