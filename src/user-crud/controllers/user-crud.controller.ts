import {
  Controller,
  Get,
  Post,
  Query,
  Body,
  Put,
  Delete,
  Param,
} from '@nestjs/common';
import { IGetRequest } from '../dto/request.interface';
import { IHit } from '../../user-crud/dto/posts.interface';
import { UserCrudRepository } from '../repositories/user-crud.repository';
import { ApiBody, ApiQuery } from '@nestjs/swagger';
import { DeleteHit, Hit } from '../dto/body.dto';

@Controller('user-crud')
export class UserCrudController {
  constructor(private readonly userCrudRepository: UserCrudRepository) {}

  @Get()
  @ApiQuery({
    name: '_tags',
    type: 'string',
    required: false,
  })
  @ApiQuery({
    name: 'page',
    type: 'number',
    required: false,
  })
  @ApiQuery({
    name: 'author',
    type: 'string',
    required: false,
  })
  @ApiQuery({
    name: 'title',
    type: 'string',
    required: false,
  })
  async findAll(@Query() iGetRequest: IGetRequest) {
    return await this.userCrudRepository.findAll(iGetRequest);
  }

  @Post('create')
  @ApiBody({
    type: Hit,
    required: false,
  })
  async create(@Body() post: IHit) {
    return await this.userCrudRepository.create(post);
  }

  @Delete('delete')
  @ApiBody({
    type: DeleteHit,
  })
  async delete(@Body() _id: string) {
    return await this.userCrudRepository.delete(_id);
  }
}
